package com.revolut.rates.view

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.airbnb.epoxy.TypedEpoxyController
import com.revolut.rates.data.Rate
import com.revolut.rates.model.Currency
import com.revolut.rates.model.CurrencyCalculator
import com.revolut.rates.model.CurrencyMapper
import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil
import java.io.Serializable
import java.text.ParseException

class CurrenciesController(
    private val currencyMapper: CurrencyMapper,
    private val currencyCalculator: CurrencyCalculator
) : TypedEpoxyController<List<Currency>>() {

    // Instance state key
    private val dataKey = "data"

    // Custom callbacks
    lateinit var onItemClicked: () -> Unit
    lateinit var onImeActionDone: () -> Unit

    val baseCurrency: Currency?
        get() = currentData?.find { it.isBase }
    val isInitialData: Boolean
        get() = currentData.isNullOrEmpty()

    var activeCurrency: Currency? = null

    override fun buildModels(data: List<Currency>?) {
        data?.forEach { currency ->
            CurrencyModel_()
                .id(currency.code)
                .title(currency.code)
                .subTitle(currency.name)
                .flagResId(currency.flagResId)
                .baseBg(currency.isBase)
                .amountFormat(currency.amountFormat)
                .itemInputActionListener { input, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // Clear focus after action done
                        (input as EditText).clearFocus()
                        onImeActionDone()
                        return@itemInputActionListener true
                    }

                    false
                }
                .itemInputFocusListener(object : CurrencyModel.CurrencyFocusListener() {
                    override fun onFocusChange(input: View?, hasFocus: Boolean) {
                        activeCurrency = if (hasFocus) {
                            input?.post {
                                (input as EditText).setSelection(input.text.length)
                            }
                            currentData?.get(position)
                        } else null
                    }
                })
                .itemInputTextWatcher(object : CurrencyModel.CurrencyTextWatcher() {
                    override fun afterTextChanged(s: Editable?) {
                        if (input.isFocused) {
                            // Calculate active currency amount and store it
                            activeCurrency = currentData?.get(position)
                            try {
                                activeCurrency?.amount = input.getNumericValue()
                            } catch (e: ParseException) {
                                activeCurrency?.amount = 0.0
                            }
                            if (activeCurrency === baseCurrency) baseCurrency?.let {
                                currencyCalculator.calculateAmount(it.amount, it)
                            }

                            // Update other currencies according to the active amount
                            currentData?.forEach {
                                if (it !== activeCurrency) {
                                    if (it === baseCurrency) {
                                        // Base currency has another formula to calculate an amount
                                        activeCurrency?.let { inputCurrency ->
                                            currencyCalculator.calculateBaseAmountDiff(
                                                it, inputCurrency
                                            )
                                        }
                                    } else currencyCalculator.calculateAmount(
                                        activeCurrency?.amount ?: 0.0, it
                                    )
                                }
                            }

                            update()
                        }
                    }
                })
                .onBind { model, _, position ->
                    model.itemInputTextWatcher().position = position
                    model.itemInputFocusListener().position = position
                }
                .itemClickListener { _, parentView, _, position ->
                    currentData?.toMutableList()?.apply {
                        // Reset base currency
                        forEach { it.isBase = false }

                        // Set clicked currency as base
                        val moveItem = removeAt(position)
                        moveItem.isBase = true

                        add(0, moveItem)
                        setData(this)
                        moveModel(position, 0)
                    }

                    // Show keyboard on base currency input
                    parentView.input.apply {
                        requestFocus()
                        UIUtil.showKeyboard(context, this)
                    }
                    currentData?.let { onItemClicked() }

                    activeCurrency = baseCurrency
                }
                .addTo(this)
        }
    }

    // Set latest rates in case if its initial we are creating models
    // with additional calculations, after just update the rates and base flag
    fun setLatestData(data: List<Rate>) {
        if (isInitialData) {
            setData(currencyMapper.map(data))
            if (activeCurrency == null) activeCurrency = baseCurrency
        } else {
            currentData?.forEach { current ->
                data.find { current.code == it.code }?.let { new ->
                    current.rate = new.rate
                    current.isBase = new.isBase

                    updateCurrencyAmount(current)
                }
            }

            if (activeCurrency == null) activeCurrency = baseCurrency
            update()
        }
    }

    private fun update() = setData(currentData)

    private fun updateCurrencyAmount(currency: Currency) {
        if (currency !== activeCurrency && currency !== baseCurrency)
            currencyCalculator.calculateAmount(
                baseCurrency?.amount ?: 0.0, currency
            )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(dataKey, currentData?.toMutableList() as Serializable)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onRestoreInstanceState(inState: Bundle?) {
        super.onRestoreInstanceState(inState)
        val data = inState?.getSerializable(dataKey)
        if (data != null) setData(
            (data as MutableList<Currency>).toList()
        )
    }
}
