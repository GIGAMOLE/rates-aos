package com.revolut.rates.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName


data class LatestResponse(
    @SerializedName("baseCurrency")
    val baseCurrency: String,
    @SerializedName("rates")
    val rates: JsonObject // later we will parse it by hands
)

// Right now is only one Entity for both layers (remote / local),
// but perfectly should be splitted
@Entity
data class Rate(
    @PrimaryKey val code: String,
    var rate: Double,
    var isBase: Boolean
)
