package com.revolut.rates.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.revolut.rates.data.ApiCallback
import com.revolut.rates.data.Rate
import com.revolut.rates.model.LatestDataSource
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

class LatestViewModel(
    private val repository: LatestDataSource
) : ViewModel() {

    private val _latestRates = MutableLiveData<List<Rate>>()
    val latestRates: LiveData<List<Rate>> = _latestRates

    private val _onLoading = MutableLiveData<Any>()
    val onLoading: LiveData<Any> = _onLoading

    private val _onError = MutableLiveData<String>()
    val onError: LiveData<String> = _onError

    // Store the job to cancel inside viewmodel scope
    private var getLatestJob: Job? = null

    fun getLatest(base: String?) {
        // Cancel before the start of the new one
        cancel()

        // Launch the custom ticker to do the data fetching
        // We can use also the Channel approach, but it is simpler
        getLatestJob = viewModelScope.launch {
            while (true) {
                withContext(Dispatchers.IO) { getLatestAsync(base) }
                delay(TimeUnit.SECONDS.toMillis(1L))
            }
        }
    }

    // Get latest rates from the repository
    private fun getLatestAsync(base: String?) {
        _onLoading.postValue(Any())
        repository.retrieveLatest(base, object : ApiCallback {

            override fun onError(error: String?) {
                _onError.postValue(error)
            }

            override fun onSuccess(data: List<Rate>?) {
                data?.let {
                    _latestRates.postValue(data)
                } ?: onError(null)
            }
        })
    }

    fun cancel() {
        getLatestJob?.cancel()
    }

    override fun onCleared() {
        super.onCleared()
        cancel()
    }
}
