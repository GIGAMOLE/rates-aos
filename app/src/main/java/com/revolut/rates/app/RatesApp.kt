package com.revolut.rates.app

import android.app.Application
import coil.Coil
import coil.ImageLoader
import coil.transform.CircleCropTransformation
import coil.util.CoilUtils
import com.revolut.rates.data.ApiClient
import com.revolut.rates.data.DatabaseClient
import com.revolut.rates.data.NetworkChangeReceiver
import com.revolut.rates.model.CurrencyCalculator
import com.revolut.rates.model.CurrencyMapper
import com.revolut.rates.model.LatestDataSource
import com.revolut.rates.model.LatestRepository
import com.revolut.rates.view.CurrenciesController
import com.revolut.rates.viewmodel.LatestViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

@Suppress("unused")
class RatesApp : Application() {

    /*
    All the application structure has done from scratch
    I wanted to eliminate Dagger 2.0 and RxJava approach, because there are
    Android/Kotlin related new features available
    The ViewModel Arch / Koin and other new popular stuff is kind new for me
    With this act I want to show that I want to grow with YOU and thanks
    for this amazing opportunity to try myself in this challenge :)
    * */

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@RatesApp)
            modules(
                listOf(
                    module,
                    ApiClient.module,
                    DatabaseClient.module
                )
            )
        }

        Coil.setDefaultImageLoader {
            ImageLoader(this) {
                crossfade(true)
                okHttpClient {
                    OkHttpClient.Builder()
                        .cache(CoilUtils.createDefaultCache(this@RatesApp))
                        .build()
                }
            }
        }
    }

    // App module
    private val module = module {
        viewModel { LatestViewModel(get()) }

        single { CurrencyCalculator() }
        single { CurrencyMapper(get(), get()) }
        single { CircleCropTransformation() }

        factory { CurrenciesController(get(), get()) }
        factory { NetworkChangeReceiver(get()) }
        factory<LatestDataSource> { LatestRepository(get(), get(), get()) }
    }
}
