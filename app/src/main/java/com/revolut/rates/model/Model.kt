package com.revolut.rates.model

import java.io.Serializable

data class Currency(
    val code: String,
    val name: String,
    val flagResId: Int,
    var rate: Double,
    var isBase: Boolean
) : Serializable {
    var amount: Double = 0.0
    var amountFormat: String = ""
}
