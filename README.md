# Revolut Rates

All the application structure has done from scratch

I wanted to eliminate Dagger 2.0 and RxJava approach, because there are
Android/Kotlin related new features available

The ViewModel Arch / Koin and other new popular stuff is kind new for me  
With this act I want to show that I want to grow with YOU and thanks
for this amazing opportunity to try myself in this challenge :)

Have a nice day :)

## Getting Started

- Almost all of the code is documented(with comments)
- Added additional features and design improvements
- The tests not included

## Preview

Input:  
![](/gifs/input.gif)

Base:  
![](/gifs/base.gif)

Offline:  
![](/gifs/offline.gif)

Scroll:  
![](/gifs/scroll.gif)

States:  
![](/gifs/states.gif)

## Author

[Basil Miller](https://www.linkedin.com/in/gigamole/)  
[+380 50 569 8419](tel:380505698419)  
[gigamole53@gmail.com](mailto:gigamole53@gmail.com)

