package com.revolut.rates.view

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import coil.api.load
import coil.request.RequestDisposable
import coil.transform.CircleCropTransformation
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.cottacush.android.currencyedittext.CurrencyEditText
import com.revolut.rates.R
import org.koin.core.KoinComponent
import org.koin.core.inject

@EpoxyModelClass(layout = R.layout.item_currency)
abstract class CurrencyModel : EpoxyModelWithHolder<CurrencyModel.CurrencyHolder>(), KoinComponent {

    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var subTitle: String
    @EpoxyAttribute
    @DrawableRes
    var flagResId: Int = 0
    @EpoxyAttribute
    var baseBg: Boolean = false
    @EpoxyAttribute
    lateinit var amountFormat: String

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var itemClickListener: View.OnClickListener
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var itemInputActionListener: TextView.OnEditorActionListener
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var itemInputTextWatcher: CurrencyTextWatcher
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var itemInputFocusListener: CurrencyFocusListener

    private val circleCropTransformation: CircleCropTransformation by inject()

    override fun bind(holder: CurrencyHolder) {
        super.bind(holder)

        holder.itemClick.setOnClickListener(itemClickListener)
        holder.item.setBackgroundResource(
            if (baseBg) R.color.base else R.color.white
        )

        holder.img.tag = holder.img.load(flagResId) {
            transformations(circleCropTransformation)
        }

        holder.txtTitle.text = title
        holder.txtSubtitle.text = subTitle

        if (!holder.input.hasFocus())
            holder.input.setText(amountFormat)

        itemInputTextWatcher.input = holder.input
        holder.input.addTextChangedListener(itemInputTextWatcher)
        holder.input.setOnEditorActionListener(itemInputActionListener)
        holder.input.onFocusChangeListener = itemInputFocusListener
    }

    override fun unbind(holder: CurrencyHolder) {
        super.unbind(holder)

        holder.itemClick.setOnClickListener(null)
        (holder.img.tag as RequestDisposable).dispose()

        holder.input.removeTextChangedListener(itemInputTextWatcher)
        holder.input.setOnEditorActionListener(null)
        holder.input.onFocusChangeListener = null
    }

    class CurrencyHolder : BaseEpoxyHolder() {
        val item: ViewGroup by bind(R.id.itemCurrency)
        val itemClick: View by bind(R.id.itemCurrencyClick)
        val txtTitle: TextView by bind(R.id.itemCurrencyTxtTitle)
        val txtSubtitle: TextView by bind(R.id.itemCurrencyTxtSubtitle)
        val img: ImageView by bind(R.id.itemCurrencyImg)
        val input: CurrencyEditText by bind(R.id.itemCurrencyInput)
    }

    abstract class CurrencyTextWatcher : TextWatcher {

        lateinit var input: CurrencyEditText
        var position: Int = 0

        abstract override fun afterTextChanged(s: Editable?)

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    }

    abstract class CurrencyFocusListener : View.OnFocusChangeListener {
        var position: Int = 0
    }
}
