package com.revolut.rates.model

import android.content.Context
import com.mynameismidori.currencypicker.ExtendedCurrency
import com.revolut.rates.data.Rate

// We are using this class to map the Rate entities into Currency models
// It is mapping only on first successful request, because we don't want to
// load the currency flag / name every time
class CurrencyMapper(
    private val context: Context,
    private val currencyCalculator: CurrencyCalculator
) {

    // Default amount for the first time load
    private val defaultAmount = 100.00

    fun map(rates: List<Rate>): List<Currency> {
        return rates.map {
            // Useful class to gather currency info
            val extendedCurrency = ExtendedCurrency.getCurrencyByISO(it.code)
            extendedCurrency?.loadFlagByCode(context)

            val currency = Currency(
                code = it.code,
                name = """${extendedCurrency?.name ?: it.code} - ${extendedCurrency.symbol}""",
                flagResId = extendedCurrency?.flag ?: 0,
                rate = it.rate,
                isBase = it.isBase
            )
            currencyCalculator.calculateAmount(defaultAmount, currency)

            currency
        }
    }
}
