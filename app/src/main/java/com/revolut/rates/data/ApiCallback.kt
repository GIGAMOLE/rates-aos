package com.revolut.rates.data

interface ApiCallback {
    fun onSuccess(data: List<Rate>?)
    fun onError(error: String?)
}
