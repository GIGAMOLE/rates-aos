package com.revolut.rates.model

import java.math.RoundingMode
import java.text.DecimalFormat

class CurrencyCalculator {

    // Lets out '0' inside the format to avoid '.0' this situation
    private val currencyFormat = DecimalFormat("###,##0.##").apply {
        roundingMode = RoundingMode.FLOOR
    }

    // Calculates the amount based on the base amount
    fun calculateAmount(baseAmount: Double, currency: Currency) {
        val amount = baseAmount * currency.rate

        currency.amount = amount
        currency.amountFormat = currencyFormat.format(amount)
    }

    // Calculates the amount diff for the base amount
    fun calculateBaseAmountDiff(baseCurrency: Currency, activeCurrency: Currency) {
        val amount = activeCurrency.amount / activeCurrency.rate

        baseCurrency.amount = amount
        baseCurrency.amountFormat = currencyFormat.format(amount)
    }
}
