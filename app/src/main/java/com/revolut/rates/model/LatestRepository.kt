package com.revolut.rates.model

import com.revolut.rates.data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LatestRepository(
    private val apiService: ApiClient.ApiService,
    private val rateDao: RateDao,
    private val networkChecker: ApiClient.NetworkChecker
) : LatestDataSource {

    private val defaultBaseRate = 1.0
    private var call: Call<LatestResponse>? = null

    override fun retrieveLatest(base: String?, callback: ApiCallback) {
        if (networkChecker.isAvailable()) retrieveLatestRemote(base, callback)
        else retrieveLatestLocal(base, callback)
    }

    // Starts the job to repeat rates download every second
    private fun retrieveLatestRemote(base: String?, callback: ApiCallback) {
        // Cancel the previous one if it still works
        call?.let {
            if (!it.isExecuted) it.cancel()
        }

        call = apiService.latest(base)
        call?.enqueue(object : Callback<LatestResponse> {
            override fun onFailure(call: Call<LatestResponse>, t: Throwable) {
                // Pass the error only when network is available and
                // when the call is not canceled
                if (networkChecker.isAvailable() && !call.isCanceled) callback.onError(t.message)
            }

            override fun onResponse(
                call: Call<LatestResponse>, response: Response<LatestResponse>
            ) {
                val body = response.body()

                if (response.isSuccessful && body != null) {
                    val data = parseLatestResponse(body)

                    callback.onSuccess(data)
                    // Save latest rates
                    rateDao.insertAll(data)
                } else callback.onError(response.message())
            }
        })
    }

    // Parse the LatestResponse by getting the inner Object as Map
    // Better way is to use custom Gson deserializer, but time constraints :)
    private fun parseLatestResponse(
        latestResponse: LatestResponse
    ) = arrayListOf<Rate>().apply {
        add(
            Rate(latestResponse.baseCurrency, defaultBaseRate, true)
        )

        latestResponse.rates.entrySet().forEach {
            add(
                Rate(it.key, it.value.asDouble, false)
            )
        }
    }

    private fun retrieveLatestLocal(base: String?, callback: ApiCallback) {
        try {
            val rates = rateDao.getAll().toMutableList()

            if (base != null) {
                var baseRate = defaultBaseRate

                // Reset base currency
                rates.forEach {
                    it.isBase = false
                    if (it.code == base) {
                        it.isBase = true

                        baseRate = it.rate
                        return@forEach
                    }
                }

                // Recalculate the rates according to the base rate, as API does
                rates.forEach {
                    it.rate = it.rate / baseRate
                }
            }

            callback.onSuccess(rates)
        } catch (e: Exception) {
            callback.onError(e.toString())
        }
    }
}
