package com.revolut.rates.model

import com.revolut.rates.data.ApiCallback

interface LatestDataSource {
    fun retrieveLatest(base: String?, callback: ApiCallback)
}
