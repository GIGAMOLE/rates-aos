package com.revolut.rates.view

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.math.MathUtils
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.revolut.rates.R
import com.revolut.rates.data.NetworkChangeReceiver
import com.revolut.rates.viewmodel.LatestViewModel
import kotlinx.android.synthetic.main.activity_main.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val latestViewModel: LatestViewModel by viewModel()
    private val currenciesController: CurrenciesController by inject()
    private val networkChangeReceiver: NetworkChangeReceiver by inject()

    private lateinit var offlineSnackbar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // First restore the saved controller data
        restoreControllerState(savedInstanceState)

        // Then fire the view model
        setupViewModel()
        setupUI()

        // And register the network change listener
        registerNetworkChangeReceiver()
    }

    override fun onResume() {
        super.onResume()
        getLatest()
    }

    override fun onPause() {
        super.onPause()
        latestViewModel.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterNetworkChangeReceiver()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        currenciesController.onSaveInstanceState(outState)
    }

    private fun restoreControllerState(inState: Bundle?) {
        currenciesController.onRestoreInstanceState(inState)
    }

    private fun setupViewModel() {
        latestViewModel.latestRates.observe(this, Observer {
            setState(State.SUCCESS)
            currenciesController.setLatestData(it)
        })
        latestViewModel.onError.observe(this, Observer { message ->
            if (currenciesController.isInitialData) setState(State.ERROR)
            else showMessageError(message)
        })
        latestViewModel.onLoading.observe(this, Observer {
            if (currenciesController.isInitialData) setState(State.LOADING)
        })

        offlineSnackbar = Snackbar.make(
            mainCoordinator, R.string.error_no_network, Snackbar.LENGTH_INDEFINITE
        ).apply {
            setBackgroundTint(
                ContextCompat.getColor(this@MainActivity, R.color.offline)
            )
        }
        networkChangeReceiver.onNetworkChanged = callback@ ({ isAvailable ->
            if (!isAvailable && offlineSnackbar.isShown) return@callback

            if (isAvailable) {
                offlineSnackbar.dismiss()
                getLatest()
            } else {
                offlineSnackbar.show()
                latestViewModel.cancel()
            }
        })
    }

    private fun setupUI() {
        // Register keyboard event listener
        KeyboardVisibilityEvent.registerEventListener(
            this, object : KeyboardVisibilityEventListener {
                override fun onVisibilityChanged(isOpen: Boolean) {
                    if (!isOpen) {
                        currentFocus?.clearFocus()
                        mainResetFocus.requestFocus()
                    }
                }
            }
        )

        currenciesController.onItemClicked = {
            mainCurrencies?.post {
                mainCurrencies?.let {
                    (it.layoutManager as LinearLayoutManager)
                        .scrollToPositionWithOffset(0, 0)
                }
            }

            getLatest()
        }
        currenciesController.onImeActionDone = {
            UIUtil.hideKeyboard(this)
        }

        mainCurrencies.setOnTouchListener { _, _ ->
            UIUtil.hideKeyboard(this)
            false
        }
        mainCurrencies.setHasFixedSize(true)
        mainCurrencies.setController(currenciesController)

        setupShadow()

        mainBtnReload.setOnClickListener { getLatest() }
    }

    // Fancy shadow effect on top on the scroll
    private fun setupShadow() {
        val shadowHeight = resources.getDimension(R.dimen.main_shadow_height)
        mainCurrencies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val scrollY = recyclerView.computeVerticalScrollOffset()
                val scrollFraction = MathUtils.clamp(
                    scrollY.toFloat() / shadowHeight, 0.0F, 1.0F
                )

                mainTopShadow.translationY = -shadowHeight + scrollFraction * shadowHeight
                mainTopShadow.alpha = scrollFraction
            }
        })
    }

    // Show custom error message after initial response
    private fun showMessageError(message: String?) {
        val errorMessage = message?.let {
            resources.getString(R.string.error_message, message)
        } ?: resources.getString(R.string.error_message_default)

        Snackbar.make(mainCoordinator, errorMessage, Snackbar.LENGTH_LONG).apply {
            setBackgroundTint(
                ContextCompat.getColor(this@MainActivity, R.color.dark)
            )
        }.show()
    }

    private fun getLatest() = latestViewModel.getLatest(
        currenciesController.baseCurrency?.code
    )

    private fun setState(state: State) {
        val index = state.ordinal

        if (mainStates.displayedChild == index) return
        mainStates.displayedChild = index
    }

    // We can use registerNetworkCallback() from ConnectivityManager also
    @Suppress("DEPRECATION")
    private fun registerNetworkChangeReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) registerReceiver(
            networkChangeReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    private fun unregisterNetworkChangeReceiver() {
        try {
            unregisterReceiver(networkChangeReceiver)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    // States for the initial main view
    enum class State {
        LOADING, ERROR, SUCCESS
    }
}
