package com.revolut.rates.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import org.koin.dsl.module

object DatabaseClient {

    private const val DATABASE_NAME = "rate_database"

    // DB module
    val module = module {
        single { provideRateDatabase(get()) }
        single { get<RateDatabase>().rateDao() }
    }

    private fun provideRateDatabase(context: Context) = Room.databaseBuilder(
        context.applicationContext,
        RateDatabase::class.java,
        DATABASE_NAME
    )
        // Since our currency list is small and due to time limits,
        // I used this function to allow doing DB commands easily
        .allowMainThreadQueries()
        .build()

    @Database(entities = [Rate::class], version = 1, exportSchema = false)
    abstract class RateDatabase : RoomDatabase() {
        abstract fun rateDao(): RateDao
    }
}
