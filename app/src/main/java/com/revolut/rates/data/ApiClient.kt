package com.revolut.rates.data

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


object ApiClient {

    private const val API_BASE_URL = "https://hiring.revolut.codes/api/android/"

    // Network module
    val module = module {
        factory { provideLoggingInterceptor() }
        factory { provideOkHttpClient(get()) }
        factory { provideApi(get()) }
        factory { provideNetworkChecker(get()) }
        single { provideRetrofit(get()) }
    }

    private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private fun provideLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        // this line forces eliminating SocketTimeoutException after the network config changes
        .protocols(listOf(Protocol.HTTP_1_1))
        .addInterceptor(loggingInterceptor)
        .build()

    private fun provideApi(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    // Check the internet connection
    @Suppress("DEPRECATION")
    private fun provideNetworkChecker(context: Context): NetworkChecker = object : NetworkChecker {
        override fun isAvailable(): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

            return if (connectivityManager != null) {
                val activeNetwork = connectivityManager.activeNetworkInfo
                activeNetwork != null && activeNetwork.isConnectedOrConnecting
            } else false
        }
    }

    interface NetworkChecker {
        fun isAvailable(): Boolean
    }

    interface ApiService {
        @GET("latest")
        fun latest(@Query("base") base: String?): Call<LatestResponse>
    }
}
